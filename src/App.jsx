import { useState } from 'react'
import './App.css'

function App() {
    const [count, setCount] = useState(0);

    const increaseButton = () => {
      setCount(count +1);
    };

    const decreaseButton = () => {
      if (count > 0) {
        setCount(count -1);
      }
    };

    const resetButton = () => {
      setCount(0);
    };

  return (
    <>
      <h1>Compteur des participants: {count} </h1>
<button onClick={increaseButton} className='increase'> Participer à l'activité </button>
<button onClick={decreaseButton} className='decrease'> Ne plus participer </button>
<button onClick={resetButton} className='reset'> Remettre le compteur à zéro </button>
    </>
  )
}

export default App
